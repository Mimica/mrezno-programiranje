#zadatak_7 - primjer 1.py 
#- coding: utf-8 --
from multiprocessing import Process, Queue
import socket
import datetime
import thread
import time
import random
from local_machine_info import print_machine_info

print "Vrijeme Pokretanja:"
print datetime.datetime.now()

print "Podaci ovog racunala:"
print_machine_info()

def rand_num():
	num = random.random()
	print "\n %f" % num

if __name__ == "__main__":
	queue = Queue()
	
	processes = [Process(target=rand_num, args=() ) for x in range(4)]
	
	for p in processes:
		p.start()
		
	for p in processes:
		p.join
