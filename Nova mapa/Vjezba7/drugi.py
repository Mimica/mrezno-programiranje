#zadatak_7 - primjer 2.py 
#-- coding: utf-8 --
from multiprocessing import Process, Queue
import multiprocessing as mp
import socket
import datetime
import thread
import time
import random
from local_machine_info import print_machine_info

print "Vrijeme Pokretanja:"
print datetime.datetime.now()

print "Podaci ovog racunala:"
print_machine_info()


def my_func(x):
	print (x**x)
def main():
	pool = mp.Pool(mp.cpu_count())
	result = pool.map(my_func, [4,2,3])

if __name__ == "__main__":
	main()
