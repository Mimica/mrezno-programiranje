#Vjezba 7

from multiprocessing import Pool
import multiprocessing as mp
import socket
import sys
import datetime
from local_machine_info import print_machine_info

def host_to_ip(host):
    print "Resolving", host
    return socket.gethostbyname(host)


def scan(target):
    target_ip, port = target

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(2)

    try:
        sock.connect((target_ip, port))
        sock.close()

        return port, True
    except (socket.timeout, socket.error):
        return port, False

if __name__ == '__main__':
	print "Vrijeme Pokretanja:"
	print datetime.datetime.now()

	print "Podaci ovog racunala:"
	print_machine_info()
	

	host = raw_input("Unesi adresu hosta kojeg zelite testirati: ")
	print("Unesite od kojeg do kojeg porta zelite napraviti skeniranje:")
	send_firstport = raw_input("Unesi prvi port: ")
	send_lastport = raw_input("Unesi zadnji port: ")

    # Resolve Host to IP, if necessary.
	if not host.replace(".", "").isdigit():
		host = host_to_ip(host)
		print "Scanning", host
	first = int(send_firstport)
	last = int (send_lastport)
	ports = range(int(first), int(last)+1)
	scanlist = [(host, port) for port in ports]

	pool = Pool(mp.cpu_count()*2)
	n = 0;
	for port, status in pool.imap(scan, scanlist):
		if status:
			print port, "is open!"
			n = n +1;
	print "Finished scanning", host
	print n, "ports are open"
