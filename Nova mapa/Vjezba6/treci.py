#zadatak_6 - primjer 3.py 
#-- coding: utf-8 --
import socket
import datetime
import thread
import threading
import time
from local_machine_info import print_machine_info



print "Vrijeme Pokretanja:"
print datetime.datetime.now()

print "Podaci ovog racunala:"
print_machine_info()



class myThread (threading.Thread):
	def __init__(self, threadID, name, counter):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.counter = counter
	def run(self):
		print "Pokrecem nit " + self.name
		#ostavi lock zbog sinkronizacije niti
		threadLock.acquire()
		print_time(self.name, self.counter, 3)
		#oslobodi lock da bi se izvrsila sljedeca nit
		threadLock.release()

def print_time(threadName,  delay, counter):
	while counter:
		time.sleep(delay)
		print "%s: %s" % (threadName, time.ctime(time.time()))
		counter -= 1


threadLock = threading.Lock()
threads = []
	
#kreiraj nove niti
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

#pokreni nove niti
thread1.start()
thread2.start()

#dodaj niti u thread listu sa svim nitima
threads.append(thread1)
threads.append(thread2)

#cekaj dok se sve niti ne izvrse

for t in threads:
	t.join()

print "\nIZlazim iz glavne niti\n"