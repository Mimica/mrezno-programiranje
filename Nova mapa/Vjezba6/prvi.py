#zadatak_6 - primjer 1.py 
#- coding: utf-8 --
import socket
import datetime
import thread
import time
from local_machine_info import print_machine_info



print "Vrijeme Pokretanja:"
print datetime.datetime.now()

print "Podaci ovog racunala:"
print_machine_info()

#Definicija funkcije za nit
def print_time( threadName,delay):
	count = 0
	while count < 5:
		time.sleep(delay)
		count += 1
		print "%s: %s" % ( threadName, time.ctime(time.time()))
		
#Kreiramo dvije niti
try:
	thread.start_new_thread( print_time, ("thread-1", 2, ) )
	thread.start_new_thread( print_time, ("thread-2", 4, ) )
except:
	print "Greska: ne mogu pokrenuti nit!!"
	
#Čekaj dok se sve niti ne izvrše
while 1:
	pass